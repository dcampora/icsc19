import tools
from dataset import dataset

class instance(object):
  def __init__(self,
    combine=True,
    balance=True,
    balance_fraction=0.5,
    particle_ranges=((0.0, 0.8), (0.8, 0.9), (0.9, 1.0)),
    base_containing_folder="/localprojects/dcampora/cnn_input_20181112/",
    image_size=32,
    image_format="binary"):
    self.balance = balance
    self.balance_fraction = balance_fraction
    # Load training and validation data
    # Balance training dataset
    self.training_dataset = dataset(datatype="training",
      balance=balance,
      balance_fraction=balance_fraction,
      particle_range=particle_ranges[0],
      base_containing_folder=base_containing_folder,
      image_size=image_size,
      image_format=image_format)

    self.validation_dataset = dataset(datatype="validation",
      particle_range=particle_ranges[1],
      base_containing_folder=base_containing_folder,
      image_size=image_size,
      image_format=image_format)

    self.test_dataset = dataset(datatype="test",
      particle_range=particle_ranges[2],
      base_containing_folder=base_containing_folder,
      image_size=image_size,
      image_format=image_format)

    # Print some statistics
    print(self.training_dataset.statistics_string())
    print(self.validation_dataset.statistics_string())
    print(self.test_dataset.statistics_string())
  
  def solve(self, model):
    """Solves the problem instance by applying the model
    passed by parameter.
    """
    model.run(self.training_dataset.dataset["x"], self.training_dataset.dataset["y"], 
      self.validation_dataset.dataset["x"], self.validation_dataset.dataset["y"],
      self.test_dataset.dataset["x"], self.test_dataset.dataset["y"])
    
    print('\nTest loss:', model.score[0])
    print('Test accuracy:', model.score[1])
    print('\n' + model.efficiencies_string())
    print(model.identification_string())

    # Save the generated model
    model.save("saved_models/",
      self.training_dataset,
      self.validation_dataset,
      self.test_dataset,
      self.balance,
      self.balance_fraction
    )
