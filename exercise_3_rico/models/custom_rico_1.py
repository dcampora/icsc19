from models.model import keras_model
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D

class custom_rico_1_model(keras_model):
  def __init__(self,
    input_shape,
    batch_size = 128,
    num_classes = 6,
    epochs = 100):
    super(custom_rico_1_model, self).__init__(batch_size, num_classes, epochs)

    self.model = Sequential()
    self.model.add(Conv2D(32, (3, 3), padding='same',
                     input_shape=input_shape))
    self.model.add(Activation('relu'))
    self.model.add(Conv2D(32, (3, 3), padding='same'))
    self.model.add(Activation('relu'))
    self.model.add(MaxPooling2D(pool_size=(2, 2)))

    self.model.add(Conv2D(64, (3, 3), padding='same'))
    self.model.add(Activation('relu'))
    self.model.add(Conv2D(64, (3, 3), padding='same'))
    self.model.add(Activation('relu'))
    self.model.add(MaxPooling2D(pool_size=(2, 2)))
    
    self.model.add(Conv2D(128, (3, 3), padding='same'))
    self.model.add(Activation('relu'))
    self.model.add(Conv2D(128, (3, 3), padding='same'))
    self.model.add(Activation('relu'))
    
    self.model.add(Flatten())
    self.model.add(Dense(256))
    self.model.add(Activation('relu'))
    self.model.add(Dropout(0.25))

    self.model.add(Dense(16))
    self.model.add(Activation('relu'))
    self.model.add(Dropout(0.25))

    self.model.add(Dense(num_classes))
    self.model.add(Activation('softmax'))
    
    self.model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

  def __repr__(self):
    return "custom_rico_1"
