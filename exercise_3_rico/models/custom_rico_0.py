from models.model import keras_model
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D

class custom_rico_0_model(keras_model):
  def __init__(self,
    input_shape,
    batch_size = 128,
    num_classes = 6,
    epochs = 100):
    super(custom_rico_0_model, self).__init__(batch_size, num_classes, epochs)

    self.model = Sequential()
    self.model.add(Conv2D(32, (3, 3), padding='same',
                     input_shape=input_shape))
    self.model.add(Activation('relu'))
    self.model.add(Conv2D(32, (3, 3), padding='same'))
    self.model.add(Activation('relu'))
    self.model.add(Conv2D(32, (3, 3), padding='same'))
    self.model.add(Activation('relu'))

    self.model.add(Flatten())
    self.model.add(Dense(256))
    self.model.add(Activation('relu'))
    
    self.model.add(Dropout(0.5))
    self.model.add(Dense(num_classes))
    self.model.add(Activation('softmax'))
    
    self.model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

    # Add early stop regularization technique
    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=3, verbose=1, mode='auto')
    self.callbacks.append(early_stop)

  def __repr__(self):
    return "custom_rico_0"
