classification = {
  "electron": 0,
  "pion": 1,
  "muon": 2,
  "kaon": 3,
  "proton": 4,
  "deuteron": 5
}

heavy_particles = ["proton", "kaon", "deuteron"]
light_particles = ["pion", "muon", "electron"]
